package com.pentatools.serviceorder.domain.ports.in;

import com.pentatools.serviceorder.common.exception.GlobalException;
import com.pentatools.serviceorder.common.exception.ParameterNotFoundException;
import com.pentatools.serviceorder.domain.model.Order;

public interface CreateOrderUseCase {
    void createOrder(Order order) throws ParameterNotFoundException, GlobalException;
}