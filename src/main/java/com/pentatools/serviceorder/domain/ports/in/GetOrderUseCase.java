package com.pentatools.serviceorder.domain.ports.in;

import com.pentatools.serviceorder.common.exception.GlobalException;
import com.pentatools.serviceorder.common.exception.ResourceNotFoundException;
import com.pentatools.serviceorder.domain.model.Order;

public interface GetOrderUseCase {
    Order getOrder(String orderId) throws ResourceNotFoundException, GlobalException;
}
